#include<time.h>
#include<iostream>
#include<cstdio>
#include<stdlib.h>
#include<cmath>
#include<windows.h>
#include<conio.h>
#define PI 3.14159265
using namespace std;
void hands(char clockface[][101],int clockthickness, float radius,float x, float y);
int main()
{

int clockthickness = 2; //grubosc
int yaxis, xaxis;//osie
char clockface[101][101]; //tarcza zegara
time_t timer; //czas w sekundach od 1.01.1970
struct tm *timeinfo; //aktualna data
time(&timer);
timeinfo=localtime(&timer);
int hour = timeinfo->tm_hour;
int archour = 30*hour;
int minutes = timeinfo->tm_min;
int arcmin = 6*minutes;
printf ("Current local time and date: %s", asctime(timeinfo));
HANDLE  hColor;
hColor = GetStdHandle (STD_OUTPUT_HANDLE);
int radius =24;
//zakropkowanie tarczy zegara
for(yaxis=0;yaxis<=radius*2+1;yaxis++)
{
    for(xaxis=0;xaxis<=radius*3+1;xaxis++)
    {
        clockface[xaxis][yaxis]='.';
    }
}

//rysowanie kola z iksow
for(yaxis=0;yaxis<=radius*2+1;yaxis++)
{
    for(xaxis=0;xaxis<=radius*3+1;xaxis++)
    {
        if(sqrt(pow(xaxis*2/3-radius,2)+ pow(yaxis-radius,2))+clockthickness>=radius &&sqrt(pow(xaxis*2/3-radius,2)+ pow(yaxis-radius,2))<=radius) clockface[xaxis][yaxis]='X';
    }
}

//wskazowki
hands( clockface,7*clockthickness,  radius, radius+radius*sin(PI*archour/180),radius+radius*cos(PI*archour/180));
hands( clockface,4*clockthickness,  radius, radius+radius*sin(PI*arcmin/180),radius+radius*cos(PI*arcmin/180));
//wyswietlenie zegara
for(yaxis=radius*2;yaxis>=0;yaxis--)
{
    for(xaxis=1;xaxis<=radius*3;xaxis++)
    {
        if(clockface[xaxis][yaxis]=='X')
        {

            SetConsoleTextAttribute(hColor, 13);
            cout<<clockface[xaxis][yaxis];
            SetConsoleTextAttribute(hColor, 7);
        }
        if(clockface[xaxis][yaxis]=='x')
        {

            SetConsoleTextAttribute(hColor, 10);
            cout<<'X';
            SetConsoleTextAttribute(hColor, 7);
        }
        if(clockface[xaxis][yaxis]=='.') cout<<clockface[xaxis][yaxis];
    }
    cout<<"\n";

}

   cout<<"Press enter to exit";
   getch();

}

void hands(char clockface[][101],int clockthickness, float radius,float x, float y)
{
    if(x!=radius)
    {
            float deltax = x-radius;
        float deltay = y-radius;
        float a=deltay/deltax;
        int xaxis=radius;
        int yaxis=radius;
        int r=radius;
       clockface[3*r/2][r]='X';
        int i=0;
        while(sqrt(pow(xaxis+i*2/3-radius,2)+ pow(yaxis-radius,2))+clockthickness<=radius)
        {
            if(deltax>=0)
            {
                i++;
                float tempvalue=radius + 2*i*a/3;
                double round(tempvalue);
                int oldyaxis=yaxis;
                yaxis=tempvalue;
                if(yaxis>=oldyaxis)
                {
                    for(;oldyaxis<=yaxis;oldyaxis++)clockface[3*(xaxis)/2+i][oldyaxis]='x';
                }
                else
                {
                    for(;oldyaxis>=yaxis;oldyaxis--)clockface[3*(xaxis)/2+i][oldyaxis]='x';
                }
            }
            else
            {
                i--;
                float tempvalue=radius + 2*i*a/3;
                double round(tempvalue);
                int oldyaxis=yaxis;
                yaxis=tempvalue;
                if(yaxis>=oldyaxis)
                {
                    for(;oldyaxis<=yaxis;oldyaxis++)clockface[3*(xaxis)/2+i][oldyaxis]='x';
                }
                else
                {
                    for(;oldyaxis>=yaxis;oldyaxis--)clockface[3*(xaxis)/2+i][oldyaxis]='x';
                }
            }
        }
    }
    else
    {
        int xaxis=radius;
        int yaxis=radius;
        clockface[3*xaxis/2][yaxis]='X';
        if(y>radius)
        {
            while(yaxis+clockthickness<=2*radius)
            {
                yaxis++;
                clockface[3*xaxis/2][yaxis]='x';
            }
        }
        else
        {
            while(yaxis-clockthickness>=0)
            {
                yaxis--;
                clockface[3*xaxis/2][yaxis]='x';
            }
        }
    }
}



